import Vue from 'vue'
import Vuex from 'vuex'
import _ from 'lodash'

Vue.use(Vuex);

// това е така да се види концепцията по нагледно,
// може да се нацепи на модули целия стор, пише си в документацията как се прави

export default new Vuex.Store({
    state: {
        items: [
            // uncomment to see the difference
            // {el: 'TextField', type: 'text', placeholder: 'Musaka', label: 'Hello Musaka'},
            // {el: 'CheckboxField', label: 'Basic 2'},
        ],
        currentItem: null
    },
    mutations: {
        'addItem': (state, item) => {
            state.items.push(item)
        },
        'editItem': (state, index) => {
            state.currentItem = index;
        },
        'removeItem': (state, i) => {
            state.items = _.reduce(state.items, (result, value, index) => {
                if (index !== i) {
                    result.push(value)
                }
                return result;
            }, [])
        }
    },
    actions: {
        addItem(store, item) {
            store.commit('addItem', item)
        },
        editItem(store, index) {
            store.commit('editItem', index)
        },
        removeItem(store, index) {
            store.commit('removeItem', index)
        }
    },
    getters: {
        allItems: state => state.items,
        currentItem: state => state.currentItem,
    }
})
